#!/bin/bash
#
# !!! PLEASE RUN THIS SCRIPT FROM PROJECT ROOT !!!
# eg: bash ci-cd/initial_setup.sh
#
# This script will run after git pull to init all dependencies, create server ref log
# eg: install composer, npm, bower
#

set -e


# Make config file and get link
#if [[ ! -f "ci-cd/server_config/variables.rb" ]]; then
#    cp ci-cd/server_config/variables.example.rb ci-cd/server_config/variables.rb
#fi

# Config file for shop
#CONFIG_FILES=(
#    ".env"
#    )

#for CONFIG_FILE in ${CONFIG_FILES[@]}; do
#    if [[ ! -f $CONFIG_FILE ]]; then
#        erb -r ./ci-cd/server_config/variables $CONFIG_FILE.erb > $CONFIG_FILE
#    else
#        erb -r ./ci-cd/server_config/variables $CONFIG_FILE.erb > $CONFIG_FILE.tmp
#        if ! diff -q $CONFIG_FILE.tmp $CONFIG_FILE &>/dev/null; then
#        >&2 mv $CONFIG_FILE.tmp $CONFIG_FILE;
#        fi
#        # Remove temporary files
#        rm -f $CONFIG_FILE.tmp
#    fi

# switch vendors over
# Set composer root

echo "Step 2: Set variables for deploy"
COMPOSER_ROOT=.
COMPOSER_FILE=$COMPOSER_ROOT/composer.json
#COMPOSER_FILE_OLD=tmp/old.composer.json
WEB_ROOT=public;

#if diff $COMPOSER_FILE $COMPOSER_FILE_OLD > /dev/null; then
#echo "Step 3: Nothing changed, No need to install composer"

#else

echo "Step 3.1: Copy vendor from pre-deploy state"
rsync -rahq --delete tmp/vendor/ $COMPOSER_ROOT/vendor/
if [ -f composer.phar ] && [ -f composer.json ]; then
echo "Step 3.2: Install optimize autoloader for composer"
php composer.phar install --optimize-autoloader
#fi
fi

# Initial setup all environment

if [[ ! -f "ci-cd/server_log/nfo.txt" ]]; then
    mkdir -p ci-cd/server_log/
    touch ci-cd/server_log/nfo.txt
fi
    rm -f $WEB_ROOT/nfo.txt
if [ ! -f "$WEB_ROOT/nfo.txt" ] && [ -f "ci-cd/server_log/nfo.txt" ]; then
    ln -s `pwd`/ci-cd/server_log/nfo.txt `pwd`/$WEB_ROOT
fi

if [[ ! -f .env ]] && [[ -f .env.example ]]; then
echo "Step 5: Copy .env from example"
    cp .env.example .env
fi

#php artisan config:clear
#php artisan cache:clear
