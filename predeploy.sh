#!/bin/bash
#
# !!! PLEASE RUN THIS SCRIPT FROM PROJECT ROOT !!!
# eg: bash ci-cd/scrips/predeploy-dev.sh
#
# This script will run before git pull to make sure all required dependencies are installed
# eg: install composer, npm, bower
#

# Set exit script on error!
set -e;

#
# COMPOSER SECTION
# IF YOU DO NOT USE COMPOSER, JUST COMMENT THIS SECTION
# 

# Set composer root and back
COMPOSER_ROOT=.
COMPOSER_JSON=$COMPOSER_ROOT/composer.json
COMPOSER_LOCK=$COMPOSER_ROOT/composer.lock
#COMPOSER_JSON_OLD=tmp/old.composer.json
#COMPOSER_LOCK_OLD=tmp/old.composer.lock



# Download composer.phar if not exist
echo "Pre-deploy step 1: Download composer.phar if not exist"
if [[ ! -f "$COMPOSER_ROOT/composer.phar" ]]; then
    cd $COMPOSER_ROOT
    #curl -sS https://getcomposer.org/installer | php
    curl https://getcomposer.org/download/1.10.17/composer.phar --output composer.phar
    chmod +x composer.phar
fi

if [[ ! -d "tmp" ]]; then
echo "Pre-deploy step 2: created tmp directory"
mkdir -p tmp
else 
echo "Pre-deploy step 2: No need create tmp directory"
fi

echo "Pre-deploy step 3: checkout new composer file"

git checkout $CI_BUILD_REF -- $COMPOSER_ROOT/composer.json
git checkout $CI_BUILD_REF -- $COMPOSER_ROOT/composer.lock

#cp -f $COMPOSER_ROOT/composer.json tmp/old.composer.json
#cp -f $COMPOSER_ROOT/composer.lock tmp/old.composer.lock

echo "Pre-deploy step 4: Remove all composer file in tmp directory"

if [[ -f "tmp/composer.phar" || -f "tmp/composer.lock" || -f "tmp/composer.json" ]]; then
    rm tmp/composer.*
fi
echo "Pre-deploy step 5: create vendor directory if not exist"

mkdir -p vendor

#Install composer for first time

if [[ ! -f "vendor/autoload.php" ]];  then
echo "Pre-deploy step bonus: Install composer for first time"
php composer.phar install --optimize-autoloader
fi

echo "Pre-deploy step 6: Rsync vendor to tmp folder"
rsync -rahq --delete $COMPOSER_ROOT/vendor/ tmp/vendor/

echo "Pre-deploy step 7: Copy new composer file to tpm directory"

cp $COMPOSER_ROOT/composer.phar tmp/composer.phar
cp $COMPOSER_JSON tmp/composer.json
cp $COMPOSER_LOCK tmp/composer.lock


function revert-file () {
   FILE=$1
   FILE_PREVIOUS_COMMIT=`git show ${CI_BUILD_REF}~1:${FILE}`

   if [[ ! -z $FILE_PREVIOUS_COMMIT ]]
   then
      git checkout ${CI_BUILD_REF}~1 -- ${FILE}
   else
      echo "File not exist in previous commit or error checking ...!!!"
   fi
}

revert-file $COMPOSER_LOCK
revert-file $COMPOSER_JSON

#if [[ diff $COMPOSER_JSON $COMPOSER_JSON_OLD > /dev/null || diff $COMPOSER_LOCK $COMPOSER_LOCK_OLD > /dev/null ]]; then
#if diff $COMPOSER_JSON $COMPOSER_JSON_OLD > /dev/null && diff $COMPOSER_LOCK $COMPOSER_LOCK_OLD > /dev/null; then

#echo "Pre-deploy step 8: Nothing changed, No need to install composer"

#else

echo "Pre-deploy step 8: Install composer without autoloader"
(cd tmp; php composer.phar install --no-scripts --no-autoloader; cd ..)
#fi

echo "Pre-deploy step 9: Backup current vendor directory"
rsync -rahq --delete $COMPOSER_ROOT/vendor/ tmp/vendor_old/

#
# END COMPOSER
#
